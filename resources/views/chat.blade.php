@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                <div class="panel-heading" id="conversation" style="width: 750px; height: 500px; border: 1px solid #ccc; background-color: #eee; padding: 4px; overflow: scroll"></div>

                <div class="panel-footer">
                <form id="chatform" onsubmit="return pushChat();">
                        <input type="text" id="wisdom" size="95" placeholder="Type Here ...." value="">
                </form> 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection